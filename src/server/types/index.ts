import { RowDataPacket } from "mysql2/promise";
export interface Book {
  id?: number;
  title: string;
  author: string;
  published_date: Date;
  isbn?: string;
}
