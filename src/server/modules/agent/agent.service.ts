import { connection as db } from "../../db/mysql-connection";
import { OkPacket } from "mysql2/promise";
import {
  Agent,
  Place,
  AgentAndPlaces,
  AgentsAndPlaces,
} from "./agent.validation";

export const create_agent: Function = async (
  payload: Agent
): Promise<OkPacket | null> => {
  const sql = `INSERT INTO agent SET ?`;
  const results = await db.query(sql, payload);
  const result: OkPacket = results[0] as OkPacket;
  if (result.affectedRows > 0) return result;

  return null; // return null if the request failed
};

export const create_place: Function = async (
  payload: Place
): Promise<OkPacket | null> => {
  const sql = `INSERT INTO place SET ?`;
  const results = await db.query(sql, payload);
  const result: OkPacket = results[0] as OkPacket;
  if (result.affectedRows > 0) return result;

  return null; // return null if the request failed
};

export const get_all_agents: Function = async (): Promise<
  AgentsAndPlaces[] | null
> => {
  const sql = `SELECT agent.*, place.name "place_name", place.hits, place.agent_id
    FROM agent
    JOIN place
    ON agent.id = place.agent_id;`;
  const results = await db.query(sql);
  const rows = results[0] as any[];
  if (rows) return rows as AgentsAndPlaces[];

  return null; // return null if the request failed
};

export const get_single_agent: Function = async (
  agent_id: string
): Promise<AgentAndPlaces | null> => {
  const sql_get_agent = `SELECT * FROM agent WHERE id = '${agent_id}'`;
  const resultsAgent = (await db.query(sql_get_agent)) as any[];
  let agent = resultsAgent[0][0];
  const sql_get_agent_with_places = `SELECT * FROM agent
  JOIN place
  ON agent.id = place.agent_id
  WHERE agent.id = '${agent_id}';`;
  const resultsAgentAndPlaces = (await db.query(
    sql_get_agent_with_places
  )) as any[];
  let agentAndPlaces = {
    ...agent,
    places: [],
  };
  resultsAgentAndPlaces[0].forEach((currRow: any) => {
    const { name, hits, agent_id } = currRow;
    const currPlace: Place = {
      name,
      hits,
      agent_id,
    };
    agentAndPlaces.places.push(currPlace);
  });
  if (agentAndPlaces) return agentAndPlaces as AgentAndPlaces;

  return null; // return null if the request failed
};
