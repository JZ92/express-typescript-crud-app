/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route-async-wrapper";
import express, { Request, Response } from "express";
import * as service from "./agent.service";
import {
  Agent,
  Place,
  validate_agent,
  validate_place,
} from "./agent.validation";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW AGENT
router.post(
  "/agent",
  raw(
    async (req: Request, res: Response): Promise<void> => {
      const valid = validate_agent(req.body);
      if (!valid && validate_agent.errors) throw validate_agent.errors[0];
      const result = await service.create_agent(req.body as Agent);
      const ok = { status: 200, message: `Agent was created successfully` };
      const fail = { status: 404, message: `Error in creating an agent` };
      const { status, message } = result ? ok : fail;
      res.status(status).json({ message, result });
    }
  )
);

// CREATES A NEW PLACE
router.post(
  "/place",
  raw(
    async (req: Request, res: Response): Promise<void> => {
      const valid = validate_place(req.body);
      if (!valid && validate_place.errors) throw validate_place.errors[0];
      const result = await service.create_place(req.body as Place);
      const ok = { status: 200, message: `Place was created successfully` };
      const fail = { status: 404, message: `Error in creating a place` };
      const { status, message } = result ? ok : fail;
      res.status(status).json({ message, result });
    }
  )
);

// GET ALL AGENTS
router.get(
  "/",
  raw(
    async (req: Request, res: Response): Promise<void> => {
      const rows = await service.get_all_agents();
      res.status(200).json(rows);
    }
  )
);

// GETS A SINGLE AGENT
router.get(
  "/:id",
  raw(
    async (req: Request, res: Response): Promise<void> => {
      const row = await service.get_single_agent(req.params.id);
      if (!row?.id) {
        res
          .status(404)
          .json({ message: `No agent found with id of ${req.params.id}` });
        return;
      }
      res.status(200).json(row);
    }
  )
);

export default router;
