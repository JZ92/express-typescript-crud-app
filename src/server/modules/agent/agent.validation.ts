import Ajv, { JTDSchemaType } from "ajv/dist/jtd";
const ajv = new Ajv();

export interface Agent {
  name: string;
  type: string;
  owner?: string;
  status: string;
}

export interface AgentAndPlaces {
  name: string;
  type: string;
  owner?: string;
  status: string;
  places?: Place[];
}

export interface AgentsAndPlaces {
  name: string;
  type: string;
  owner?: string;
  status: string;
  place_name: string;
  hits: number;
  agent_id: number;
}

export interface Place {
  name: string;
  hits: number;
  agent_id: number;
}

const agent_schema: JTDSchemaType<Agent> = {
  properties: {
    name: { type: "string" },
    type: { type: "string" },
    status: { type: "string" },
  },
  optionalProperties: {
    owner: { type: "string" },
  },
};

const place_schema: JTDSchemaType<Place> = {
  properties: {
    name: { type: "string" },
    hits: { type: "int32" },
    agent_id: { type: "int32" },
  },
};

export const validate_agent = ajv.compile(agent_schema);
export const validate_place = ajv.compile(place_schema);
